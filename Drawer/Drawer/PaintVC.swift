//
//  ViewController.swift
//  Drawer
//
//  Created by Adygzhy Ondar on 03/05/15.
//  Copyright (c) 2015 Adygzhy Ondar. All rights reserved.
//

import UIKit

class PaintVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var helperImageView: UIImageView!
    
    var brushWidth: CGFloat = 10
    
    // last drawn point
    var lastPoint = CGPoint.zeroPoint
    
    // RGB values of selected brush colour
    // black is the default colour
    var red: CGFloat = 1.0
    var green: CGFloat = 0.5
    var blue: CGFloat = 0.5
    
    // this property is used to identify if the brush stroke is continuous.
    var swiped = false
    
    var opacity: CGFloat = 1.0
    
    /* * * * * * DRAWING * * * * * */
    // 1. start of drawing event
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        swiped = false
        if let touch = touches.first as? UITouch {
            // lastPoint is a point we touched firstly in imageView
            lastPoint = touch.locationInView(self.imageView)
        }
    }
    
    // drawing in helperImageView
    func drawLine(startPoint: CGPoint, endPoint: CGPoint) {
        // create graphics context and make it is the current context
        UIGraphicsBeginImageContext(helperImageView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        helperImageView.image?.drawInRect(CGRect(x: 0, y: 0, width: helperImageView.frame.size.width, height: helperImageView.frame.size.height))
        
        //
        CGContextMoveToPoint(context, startPoint.x, startPoint.y)
        CGContextAddLineToPoint(context, endPoint.x, endPoint.y)
        
        // set some parameters
        CGContextSetLineCap(context, kCGLineCapRound)
        CGContextSetLineWidth(context, brushWidth)
        CGContextSetRGBStrokeColor(context, red, green, blue, 1.0)      // opacity level is 1.0
        CGContextSetBlendMode(context, kCGBlendModeNormal)
        
        //
        CGContextStrokePath(context)
        
        //
        helperImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        helperImageView.alpha = opacity
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        //
        swiped = true
        if let touch = touches.first as? UITouch {
            let currentPoint = touch.locationInView(self.imageView)
            drawLine(lastPoint, endPoint: currentPoint)
            // after that
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if !swiped {
            // single point
            drawLine(lastPoint, endPoint: lastPoint)
        }
        // merge helperImageView into imageView
        UIGraphicsBeginImageContext(imageView.frame.size)
        
        // don't lose the image that already exists in imageView
        imageView.image?.drawInRect(CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height),
            blendMode: kCGBlendModeNormal, alpha: 1.0)
        
        // draw in helperImageView with needed opacity
        helperImageView.image?.drawInRect(CGRect(x: 0, y: 0, width: helperImageView.frame.size.width, height: helperImageView.frame.size.height),
            blendMode: kCGBlendModeNormal, alpha: opacity)
        
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // helperImageView should be clean to use it again
        helperImageView.image = nil
    }
    
    // for setting selected parameters in another 2VC
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toPaintModeVC" {
            let paintModeVC = segue.destinationViewController as! PaintModeVC
            paintModeVC.brushWidth = brushWidth
            paintModeVC.opacity = opacity
            paintModeVC.red = red
            paintModeVC.green = green
            paintModeVC.blue = blue
        }
    }
    
    func documentsPathForFileName(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        let path = paths[0] as! String;
        let fullPath = path.stringByAppendingPathComponent(name)
        
        return fullPath
    }
    
    @IBAction func storeImage(sender: AnyObject) {
        // 1. get the image
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.image?.drawInRect(CGRect(x: 0, y: 0,
            width: imageView.frame.size.width, height: imageView.frame.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 2. store the image
        let imageData = UIImageJPEGRepresentation(image, 1)
        let relativePath = "image_\(NSDate.timeIntervalSinceReferenceDate()).jpg"
        let path = self.documentsPathForFileName(relativePath)
        // a path of the saved image
        // println(path)
        
        imageData.writeToFile(path, atomically: true)
        NSUserDefaults.standardUserDefaults().setObject(relativePath, forKey: "path")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    @IBAction func cleanCanvas(sender: AnyObject) {
        imageView.image = nil
    }
    
    @IBAction func unwindToPaintVC(sender: UIStoryboardSegue){
        let sourceVC = sender.sourceViewController as! PaintModeVC
        brushWidth = sourceVC.brushWidth
        opacity = sourceVC.opacity
        red = CGFloat(sourceVC.red)
        green = CGFloat(sourceVC.green)
        blue = CGFloat(sourceVC.blue)
        
    }
    
}