//
//  PaintModeVC.swift
//  Drawer
//
//  Created by Adygzhy Ondar on 03/05/15.
//  Copyright (c) 2015 Adygzhy Ondar. All rights reserved.
//

import UIKit

class PaintModeVC: UIViewController {

    //COLOR
    //width
    //lastik
    //rectangles
    
    //Save image
    
    //LaunchScreen
    //AppIcon
    //opacity
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var colorImage: UIImageView!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var opacitySlider: UISlider!
    @IBOutlet weak var opacityTextField: UITextField!
    
    var brushWidth: CGFloat = 10
    var opacity:CGFloat = 1
    var red: CGFloat = 0.5
    var green: CGFloat = 0.5
    var blue: CGFloat = 0.5

    @IBAction func apply(sender: AnyObject) {
        performSegueWithIdentifier("toPaintVC", sender: self)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateColorUI()
        updateWidthUI()
        updateOpacityUI()
    }
    
    func updateColorUI() {
        redSlider.value = Float(red)
        greenSlider.value = Float(green)
        blueSlider.value = Float(blue)
        colorImage.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    func updateWidthUI() {
        widthTextField.text = "\(brushWidth)"
    }
    
    func updateOpacityUI() {
        opacitySlider.value = Float(opacity)
        opacityTextField.text = "\(opacity)"
    }
    
    func updateColorFromSliders() {
        red = CGFloat(redSlider.value)
        green = CGFloat(greenSlider.value)
        blue = CGFloat(blueSlider.value)
        colorImage.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    

    @IBAction func sliderChanged(sender: UISlider) {
        updateColorFromSliders()
    }
    
    @IBAction func widthChanged(sender: UITextField) {
        var width = (sender.text as NSString).floatValue
        brushWidth = CGFloat(width)
        updateWidthUI()
    }
    
    @IBAction func widthSegmentChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            brushWidth = 10
            break
        case 1:
            brushWidth = 25
            break
        case 2:
            brushWidth = 50
            break
        default:
            brushWidth = 25
            break
        }
        updateWidthUI()
    }
    
    @IBAction func opacitySliderChanged(sender: UISlider) {
        opacity = CGFloat(sender.value)
        updateOpacityUI()
    }
}
